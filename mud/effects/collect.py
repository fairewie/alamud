
from .effect import Effect2
from mud.events import CollectEvent

class CollectEffect(Effect2):
    EVENT = CollectEvent

    def resolve_object(self):
        return self.resolve("this")
