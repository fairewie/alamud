# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import OpenEvent, OpenWithEvent, FrapperEvent, FrapperWithEvent


class OpenAction(Action2):
    EVENT = OpenEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "open"


class OpenWithAction(Action3):
    EVENT = OpenWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "open-with"

class FrapperAction(Action2):
    EVENT = FrapperEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "Frapper"


class FrapperWithAction(Action3):
    EVENT = FrapperWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "Frapper-with"
